from django.contrib import admin
from Auction.models import *
# Register your models here.

admin.site.register(Passage)
admin.site.register(Date)
admin.site.register(Hour)
admin.site.register(Boletus)
admin.site.register(Auction)
admin.site.register(Tender)
admin.site.register(Score)
admin.site.register(Comment)