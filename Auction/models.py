from django.db import models
from Login.models import *

import datetime
from django.utils import timezone

# Create your models here.


class Passage(models.Model):
    user = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True)
    station_departure = models.CharField(max_length=100)
    company = models.CharField(max_length=100)
    destination = models.CharField(max_length=100, default='destino default')
    seat = models.CharField(max_length=5, default='seat00')
    tier = models.IntegerField(default=1)
    date_departure = models.DateField(null=True, blank=True)
    hour_departure = models.TimeField(blank=True,  null=True)

    def __str__(self):
        return "Empresa: %s, Destino: %s" % (self.company, self.destination)


class Score(models.Model):
    user_punctuated = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True, related_name='Score')
    user_punctuation = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True, related_name='user_punctuation')
    stars = models.IntegerField(default=5)

class Comment(models.Model):
    user_commented = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True, related_name='Comment')
    user_commentator = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True, related_name='user_commentator')
    commentary = models.CharField(max_length=100)

class Date(models.Model):
    date = models.DateField(null=True, blank=True)


class Hour(models.Model):
    hour = models.TimeField(blank=True, null=True)

class Boletus(models.Model):
    id_boletus = models.IntegerField(default=0)

class Auction(models.Model):
    boletus = models.OneToOneField(Boletus, null=True, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True)
    passage = models.ForeignKey(Passage, on_delete=models.CASCADE, null=True)
    date = models.ForeignKey(Date, on_delete=models.CASCADE, null=True)
    hour = models.ForeignKey(Hour, on_delete=models.CASCADE, null=True)
    date_init = models.DateField(default=timezone.now)
    hour_init = models.TimeField(default='00:00:00')
    price_init = models.IntegerField(default=0)
    
    def __str__(self):
        return "Nombre: %s, precio inicial: %d" % (self.user.user.first_name, self.price_init)

class Tender(models.Model):
    boletus = models.ForeignKey(Boletus, on_delete=models.CASCADE, null=True)
    date = models.ForeignKey(Date, on_delete=models.CASCADE, null=True)
    hour = models.ForeignKey(Hour, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(Userprofile, on_delete=models.CASCADE, null=True)
    price_add = models.IntegerField(default=0)
    
    def __str__(self):
        return "Nombre: %s, precio a agregar: %d" % (self.user.user.first_name, self.price_add)
