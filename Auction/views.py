from django.shortcuts import render
from Auction.models import *
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import time


@login_required(login_url='/login/')
def Subasta(request):
    template = 'subasta.html'
    data = {}
    list_auctions = []
    for x in Auction.objects.all():
        if str(time.strftime("%Y-%m-%d")) < str(x.date.date):
            list_auctions.append(x)
        else:
            if str(time.strftime("%Y-%m-%d")) == str(x.date.date):
                if not time.strftime("%H:%M:%S") > str(x.hour.hour):
                    list_auctions.append(x)
    data['auctions'] = list_auctions

    return render(request, template, data)

@login_required(login_url='/login/')
def Mis_ubasta(request):
    template = 'mis_subasta.html'
    data = {}
    user = request.user
    if request.POST:
        auction_object = get_object_or_404(Auction,pk=request.POST['auction_id'])
        auction_object.delete()
        return JsonResponse({'result':True})
    list_auctions = []
    list_auctions2 = []
    for x in Auction.objects.all():
        if (user == x.user.user):
            list_auctions.append(x)

    for i in list_auctions:
        if str(time.strftime("%Y-%m-%d")) < str(i.date.date):
            list_auctions2.append(i)
        else:
            if str(time.strftime("%Y-%m-%d")) == str(i.date.date):
                if not time.strftime("%H:%M:%S") > str(i.hour.hour):
                    list_auctions2.append(i)
    data['auctions'] = list_auctions2
    return render(request, template, data)

@login_required(login_url='/login/')
def Ofertar(request,auction_id):
    template = 'ofertar.html'
    data = {}
    auction_object = get_object_or_404(Auction,pk=auction_id)
    if request.POST:
        if request.POST['action'] == 'comment':
            Comment.objects.create(
                                user_commented = auction_object.user,
                                user_commentator = request.user.userprofile,
                                commentary = request.POST['comentary']
                                )
            return JsonResponse({'result':True})
        elif request.POST['action'] == 'print':

            price = auction_object.price_init
            auction_object.price_init = price + int(request.POST['tender'])
            auction_object.save()
            date_now = (time.strftime("%Y/%m/%d")).replace('/','-')
            Date_object = Date.objects.create(
                                date = date_now
                                )
            hour_object = Hour.objects.create(
                                hour = time.strftime("%H:%M:%S")
                                )
            Tender.objects.create(
                                    boletus = auction_object.boletus,
                                    date = Date_object,
                                    user = request.user.userprofile,
                                    price_add = request.POST['tender'],
                                    hour = hour_object
                                    )
            return JsonResponse({'result':True})
    list_comments = []
    user_comments = auction_object.user.user
    for x in Comment.objects.all():
        if (user_comments == x.user_commented.user):
            list_comments.append(x)
    sum = 0
    cont = 1
    for x in Score.objects.all():
        if user_comments == x.user_punctuated.user:
            sum += x.stars
            cont +=1
    if cont-1 == 0:
        data['puntuation'] = 0
    else:
        data['puntuation'] = int(sum/(cont-1))
    data['comments'] = list_comments
    data['auction'] = auction_object
    data['range'] = range(5)

    return render(request, template, data)

def profileSub(request, profile_id):
    template = 'profileSub.html'
    data = {}
    profile_object = get_object_or_404(User,pk=profile_id)
    if request.POST:
        Score.objects.create(
                                user_punctuated = profile_object.userprofile,
                                user_punctuation = request.user.userprofile,
                                stars = int(request.POST['punt'])
                            )
        return JsonResponse({'result':True})
    sum = 0
    cont = 1
    for x in Score.objects.all():
        if profile_object == x.user_punctuated.user:
            sum += x.stars
            cont +=1
    if cont-1 == 0:
        data['puntuation'] = 0
    else:
        data['puntuation'] = int(sum/(cont-1))

    data['profile_id'] = profile_object
    data['range'] = range(5)
    return render(request, template, data)