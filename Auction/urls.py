from django.urls import path
from Auction import views as func_views

urlpatterns = [
    path('',func_views.Subasta, name='Subasta_subasta'),
    path('mis_subastas',func_views.Mis_ubasta, name='Subasta_mis_subasta'),
    path('profile/<int:profile_id>',func_views.profileSub, name='Subasta_profile'),
    path('oferta/<int:auction_id>',func_views.Ofertar, name='Subasta_ofertar'),
]
