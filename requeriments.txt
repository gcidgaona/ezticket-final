Django==2.0.13
Pillow==5.4.1
pytz==2019.1
qrcode==6.1
six==1.12.0
