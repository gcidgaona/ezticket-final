from django.urls import path
from Print import views as func_views

urlpatterns = [
    path('',func_views.tablePrint, name='Print_tablePrint'),
    path('print/<int:passage_id>',func_views.Print, name='Print_print'),
    path('mis_pasajes',func_views.addPasage, name='Print_mis_pasajes')
]
