from django.shortcuts import render
from Auction.models import *
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import qrcode
import time

# Create your views here.
#Example
#def example(request):
#    template = 'example.html'
#    data = {}
#    return render(request, template, data)

@login_required(login_url='/login/')
def tablePrint(request):
    template = 'tablePrint.html'
    data = {}
    user = request.user
    list_passage = []
    if request.POST:
        passage_object = get_object_or_404(Passage,pk=request.POST['passage_id'])
        passage_object.delete()
        return JsonResponse({'result':True})

    for x in Passage.objects.all():
        if (user == x.user.user):
            c=0
            for i in Auction.objects.all():
                if (x == i.passage):
                    c+=1
            if c == 0:
                list_passage.append(x)
    list_auctions2 = []
    for i in list_passage:
        if str(time.strftime("%Y-%m-%d")) < str(i.date_departure):
            list_auctions2.append(i)
        else:
            if str(time.strftime("%Y-%m-%d")) == str(i.date_departure):
                if not time.strftime("%H:%M:%S") > str(i.hour_departure):
                    list_auctions2.append(i)

    data['passages'] = list_auctions2
    return render(request, template, data)

@login_required(login_url='/login/')
def Print(request,passage_id):
    template = 'print.html'
    data = {}
    
    passage_object = get_object_or_404(Passage,pk=passage_id)
    list_passage = [
            passage_object.pk, 
            passage_object.user.user.first_name,
            passage_object.station_departure,
            passage_object.destination,
            passage_object.company
            ]
    qr = qrcode.make(list_passage)
    qr.save('Static/img/Qr.png')
    data['passage'] = passage_object
    if request.POST:
        boletus_object = Boletus.objects.create()
        date_object = Date.objects.create(
                                            date = request.POST['date']
                                            )
        hour_object = Hour.objects.create(
                                            hour = request.POST['hour']
                                            )
        Auction.objects.create(
                                boletus = boletus_object,
                                user = request.user.userprofile,
                                passage = passage_object,
                                date = date_object,
                                price_init = request.POST['price'],
                                hour = hour_object,
                                hour_init = time.strftime("%H:%M:%S")
                                )
        return JsonResponse({'result':True})
    return render(request, template, data)

def addPasage(request):
    template = "addPasage.html"
    data = {}
    if request.POST:
        for i in request.POST:
            if request.POST[i] == "":
                return JsonResponse({'result':False})

        date_object = Date.objects.create(
                                            date = request.POST['date_departure']
                                            )
        hour_object = Hour.objects.create(
                                            hour = request.POST['hour_departure']
                                            )
        Passage.objects.create(
            user = request.user.userprofile,
            station_departure = request.POST['station_departure'],
            company = request.POST['company'],
            destination = request.POST['destination'],
            seat = request.POST['seat'],
            tier = int(request.POST['tier']),
            date_departure = date_object.date,
            hour_departure = hour_object.hour
        )

        return JsonResponse({'result':True})
    return render(request, template, data)