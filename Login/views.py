from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, logout
from django.contrib.auth.models import User
from django.contrib.auth import login as loginn
from django.urls import reverse
from Login.models import *
from Auction.models import *
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
import random
import string
import time
from itertools import cycle

# Create your views here.

def validarRut(rut):
	rut = rut.upper()
	rut = rut.replace("-","")
	aux = rut[:-1]
	dv = rut[-1:]
 
	revertido = map(int, reversed(str(aux)))
	factors = cycle(range(2,8))
	s = sum(d * f for d, f in zip(revertido,factors))
	res = (-s)%11
 
	if str(res) == dv:
		return True
	elif dv=="K" and res==10:
		return True
	else:
		return False

def randomString(stringLength=4):
    letters = string.ascii_lowercase
    global CODE
    CODE = ''.join(random.choice(letters) for i in range(stringLength))

def Login(request):
    template = 'login.html'
    data = {}
    logout(request)
    if request.POST:
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        if user != None:
            loginn(request,user)
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})
    return render(request, template, data)

def Register(request):
    template = 'register.html'
    data = {}
    randomString()
    if request.POST:
        for x in request.POST:
            if request.POST[x] == "":
                return JsonResponse({'result':False})

        for i in User.objects.all():
            if (request.POST['user'] == i.username):
                return JsonResponse({'result':'username'})

        if ("@" in request.POST['user']) == False:
            return JsonResponse({'result':False})

        if not(validarRut(request.POST['rut'])):
            return JsonResponse({'result':False})
        
        
        object_user = User.objects.create_user(
                                            username = request.POST['user'],
                                            password = request.POST['pass'],
                                            first_name = request.POST['first_name'].capitalize(),
                                            last_name = request.POST['last_name'].capitalize(),
                                            is_active = True
                                        )
        Userprofile.objects.create(
                                    user = object_user,
                                    rut   = request.POST['rut'],
                                    phone = request.POST['phone'],
                                    discastatus = request.POST['discapacity']
                                )
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        loginn(request,object_user)
        send_mail(
            'Validación del correo',
            'Su codigo de verificación: ' + CODE + '',
            'ezticketdev@gmail.com',
            [request.POST['user']],
            fail_silently=False
        )
        return JsonResponse({'result':True})

    return render(request, template, data)

def Email_password(request):
    template = 'email_password.html'
    data = {}
    if request.POST:
        print (request.POST)
        users = User.objects.all()
        for user in users:
            if request.POST['email'] == str(user):
                randomString()
                send_mail(
                    'Recuperar contraseña',
                    'Su codigo es: ' + CODE + '',
                    'ezticketdev@gmail.com',
                    [request.POST['email']],
                    fail_silently=False
                )
                global user_pass
                user_pass = user
                
                return JsonResponse({'result':True})
        return JsonResponse({'result':False})

    return render(request, template, data)

def Forgot_password(request):
    template = 'forgot_password.html'
    data = {}
    if request.POST:
        user_profile =User.objects.get(username=user_pass)
        user_profile.set_password(request.POST['pass'])
        user_profile.save()
        return JsonResponse({'result':True})
    return render(request, template, data)

def Validate(request):
    template = 'validate.html'
    data = {}
    if request.POST:
        user_profile =Userprofile.objects.get(user=request.user)
        if CODE == request.POST['code']:
            user_profile.verificate = True
            user_profile.save()
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})

    return render(request, template, data)

def Validate_password(request):
    template = 'validate_password.html'
    data = {}
    if request.POST:
        print("1111111111111111111111")
        if CODE == request.POST['code']:
            print("22222222222222222222")
            return JsonResponse({'result':True})
        return JsonResponse({'result':False})
    return render(request, template, data)

@login_required(login_url='/login/')
def Dashboard(request):
    template = 'dashboard.html'
    data = {}
    user = request.user
    list_auctions = []
    list_auctions2 = []
    for x in Auction.objects.all():
        if (user == x.user.user):
            list_auctions.append(x)

    for i in list_auctions:
        if str(time.strftime("%Y-%m-%d")) < str(i.date.date):
            list_auctions2.append(i)
        else:
            if str(time.strftime("%Y-%m-%d")) == str(i.date.date):
                if not time.strftime("%H:%M:%S") > str(i.hour.hour):
                    list_auctions2.append(i)
    
    list_passage = []
    list_passage2 = []
    for y in Passage.objects.all():
        if (user == y.user.user):
            c=0
            for p in Auction.objects.all():
                if (y == p.passage):
                    c+=1
            if c == 0:
                list_passage.append(y)
    for u in Passage.objects.all():
        if (user == u.user.user):
            list_passage2.append(u)
    list_auctions3 = []
    for o in list_passage:
        if str(time.strftime("%Y-%m-%d")) < str(o.date_departure):
            list_auctions3.append(o)
        else:
            if str(time.strftime("%Y-%m-%d")) == str(o.date_departure):
                if not time.strftime("%H:%M:%S") > str(o.hour_departure):
                    list_auctions3.append(o)
    list_comments = []
    cont = 0
    all_comments = Comment.objects.all()
    all_comments = [x for x in all_comments]
    all_comments.reverse()
    for t in all_comments:
        if (user == t.user_commented.user) and (cont<3):
            list_comments.append(t)
            cont += 1
    sum = 0
    contt = 1
    for l in Score.objects.all():
        if user == l.user_punctuated.user:
            sum += l.stars
            contt +=1
    if contt-1 == 0:
        data['puntuation'] = 0
    else:
        data['puntuation'] = int(sum/(contt-1))

    data['range'] = range(5)
    data['comments'] = list_comments
    data['auctions_total'] = len(list_auctions)
    data['auctions_online'] = len(list_auctions2)
    data['passage_total'] = len(list_passage2)
    data['passage_online'] = len(list_auctions3)
    return render(request, template, data)
    
@login_required(login_url='/login/')
def Profile(request, profile_id):
    template = 'profile.html'
    data = {}
    user_profile = get_object_or_404(User,pk=profile_id)
    if request.POST:
        Comment.objects.create(
                                user_commented = user_profile.userprofile,
                                user_commentator = request.user.userprofile,
                                commentary = request.POST['comentary']
                                )
        return JsonResponse({'result':True})
    list_comments = []
    all_comments = Comment.objects.all()
    all_comments = [x for x in all_comments]
    all_comments.reverse()
    for x in all_comments:
        if (user_profile == x.user_commented.user):
            list_comments.append(x)
    data['comments'] = list_comments
    user_profile = get_object_or_404(User,pk=profile_id)
    sum = 0
    cont = 1
    for x in Score.objects.all():
        if user_profile == x.user_punctuated.user:
            sum += x.stars
            cont +=1
    if cont-1 == 0:
        data['puntuation'] = 0
    else:
        data['puntuation'] = int(sum/(cont-1))

    data['range'] = range(5)
    data['user_profile'] = user_profile
    return render(request, template, data)