from django.urls import path
from Login import views as func_views

urlpatterns = [
    path('login/',func_views.Login, name='Login_login'),
    path('',func_views.Login, name='Login_login'),
    path('register/',func_views.Register, name='Login_register'),
    path('emailPassword/',func_views.Email_password, name='Login_email_password'),
    path('forgotPassword/',func_views.Forgot_password, name='Login_forgot_password'),
    path('validate/',func_views.Validate, name='Login_validate'),
    path('validatePassword/',func_views.Validate_password, name='Login_validate_password'),
    path('dashboard/',func_views.Dashboard, name='Login_dashboard'),
    path('profile/<int:profile_id>',func_views.Profile, name='Login_profile')
]