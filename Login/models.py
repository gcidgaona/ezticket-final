from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_delete
import os

# Create your models here.
class Userprofile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rut = models.CharField(max_length=50, primary_key=True)
    imageProfile = models.ImageField(upload_to='photoprofile/%Y/%m/%d/', default='photoprofile/default-profile.png')
    phone = models.CharField(max_length=12, null=True, blank=True)
    discastatus = models.CharField(
                     max_length=1,
                     choices=(
                     ('N', 'Ninguna'),
                     ('V', 'Visual'),
                     ('A', 'Auditiva'),
                     ('M', 'Motriz'),
                     ('I', 'Intelectual'),
                     ),
                     default='N'
                     )
    verificate = models.BooleanField(default=False)
    def __str__(self):
        return "Usuario: %s, Rut: %s" % (self.user.username, self.rut)


