FROM python:3.6-jessie
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requeriments.txt /code/
RUN apt-get update
RUN apt-get install -y python \
    python-dev \
    gcc \
    python-pip \
    wget \
    libxslt-dev \
    python-imaging 
# Install requirements
RUN pip install --upgrade pip
RUN pip install -r requeriments.txt
ADD . /code/
